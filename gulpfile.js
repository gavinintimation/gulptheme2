const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

const sassFile = './style.scss';
const cssDest = './deploy/';
const jsFiles = './js/**/*.js';
const jsDest = './deploy/';

gulp.task('styles', function() {
    gulp.src(sassFile)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(cssDest));
});

gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});

gulp.task('watch', function() {
    gulp.watch(sassFile, ['styles']);
});